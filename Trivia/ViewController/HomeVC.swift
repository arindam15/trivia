//
//  HomeVC.swift
//  Trivia
//
//  Created by Arindam on 03/11/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnTestTapp(_ sender: UIButton) {
        
        let userNameVC = storyboard?.instantiateViewController(withIdentifier: "UserNameVC") as! UserNameVC
        navigationController?.pushViewController(userNameVC, animated: true)
    }
    
    @IBAction func btnTestHistoryTapp(_ sender: UIButton) {
        let testHistoryVC = storyboard?.instantiateViewController(withIdentifier: "TestHistoryVC") as! TestHistoryVC
        navigationController?.pushViewController(testHistoryVC, animated: true)
    }
}
