//
//  ViewController.swift
//  Trivia
//
//  Created by Arindam on 29/10/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit
import RealmSwift

class UserNameVC: UIViewController, UITextFieldDelegate{
    
    
    @IBOutlet weak var textfeildName: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfeildName.delegate = self
        textfeildName.returnKeyType = .done
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func btnNextTapp(_ sender: UIButton) {
        if let name = textfeildName.text, !name.isEmpty{
            let user = User(name: name)
            let test = Test(user: user, attemptedAt: getCurrentDate())
            let questions: [Question] = [
                Question(testId: test.id, question: "Who is the best cricketer in the world?",
                         answers: ["Sachin Tendulkar", "Virat Kohli", "Adam Gilcrisht", "Jacque Kalis"],
                         canSelectMultipleAnswer: false),
                
                Question(testId: test.id, question: "What are the colors in the Indian natinal flag?.",
                         answers: ["White", "Yellow", "Orange", "Green"],
                         canSelectMultipleAnswer: true),
                ]
            
            
            
            let realm = try! Realm()
            try! realm.write {
                realm.add(user)
                realm.add(test)
                realm.add(questions)
            }
            
            
            
            let testVC = storyboard?.instantiateViewController(withIdentifier: "TestVC") as! TestVC
            testVC.test = test
            navigationController?.pushViewController(testVC, animated: true)
        }
        else{
        }
    }
    
    
    fileprivate func getCurrentDate()-> String{
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy h:mm a"
        let date = Date()
        return dateFormatter.string(from: date)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}

