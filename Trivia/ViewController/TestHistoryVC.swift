//
//  TestHistoryVC.swift
//  Trivia
//
//  Created by Arindam on 04/11/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit
import RealmSwift

class TestHistoryVC: UIViewController {

    
    @IBOutlet weak var tableTestHistory: UITableView!
    var testHistory: [Test]?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableTestHistory.dataSource = self
        tableTestHistory.delegate = self
        fetchTestHistory()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }


    fileprivate func fetchTestHistory(){
        let realm = try! Realm()
        self.testHistory =  realm.objects(Test.self).map{$0}
        print(realm.objects(Test.self).first?.savedQuestions.first?.selectedAnswers)
        tableTestHistory.reloadData()
    }
    
    

}


extension TestHistoryVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (testHistory != nil) ? (testHistory!.count) : (0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let histotryCell = tableView.dequeueReusableCell(withIdentifier: "TestHistoryCell") as! TestHistoryCell
        histotryCell.selectionStyle = .none
        histotryCell.indexNumber  = indexPath.row+1
        histotryCell.testHistory = testHistory != nil ? testHistory![indexPath.row] : nil
        histotryCell.setupValue()
        return histotryCell
    }
    
    
    
}
