//
//  TestHistoryCell.swift
//  Trivia
//
//  Created by Appsbee Technology on 04/11/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit

class TestHistoryCell: UITableViewCell {

    @IBOutlet weak var labelTestTime: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelFirstQuestion: UILabel!
    @IBOutlet weak var labelFirstQuestionAnswer: UILabel!
    @IBOutlet weak var labelSecondQuestion: UILabel!
    @IBOutlet weak var labelSecondQuestionAnswer: UILabel!
    var testHistory: Test?
    var indexNumber: Int?
    public func setupValue(){
        if let testHistory = testHistory{
            labelTestTime.text = "Game \(indexNumber!) \(testHistory.attemptedAt)"
            labelUsername.text = "Name: \(testHistory.savedUser.name)"
            labelFirstQuestion.text = testHistory.savedQuestions.first?.question
            
            guard let firstAnswer = testHistory.savedQuestions.first, let secondAnswer = testHistory.savedQuestions.last else{
                return
            }
            labelFirstQuestionAnswer.text = "Answer:  \(firstAnswer.answers.joined(separator: ","))"
            labelSecondQuestion.text = testHistory.savedQuestions.last?.question
            labelSecondQuestionAnswer.text = "Answer:  \(secondAnswer.answers.joined(separator: ","))"
        }
    }
    


}
