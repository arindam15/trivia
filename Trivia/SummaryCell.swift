//
//  SummaryCell.swift
//  Trivia
//
//  Created by Sourav Santra on 04/11/19.
//  Copyright © 2019 Arindam. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell {

    
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var labelAnswer: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
